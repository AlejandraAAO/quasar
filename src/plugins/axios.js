import axios from 'axios'

const axiosInstance = axios.create({
  baseURL:'http://192.168.1.238:3200/api'
})
 export default({ Vue }) => {
   Vue.prototype.$axios = axiosInstance
 }
 
 export {axiosInstance} 

/* export default ({ Vue }) => {
  Vue.prototype.$axios = axios
} */
