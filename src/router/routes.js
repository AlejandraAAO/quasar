const Usuarios = () => import(/* webpackChunkName: "group-foo" */'pages/master/Usuarios.vue');
const Clientes =()=> import(/* webpackChunkName: "group-foo" */'pages/master/Clientes.vue');
const Sedes = ()=> import(/* webpackChunkName: "group-foo" */'pages/master/Sedes.vue');
const Rutas = ()=> import(/* webpackChunkName: "group-foo" */'pages/master/Rutas.vue');
const Carteras = ()=> import(/* webpackChunkName: "group-foo" */'pages/master/Carteras.vue');

const routes = [
  {
    path: '/dashboard', 
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: '', 
        component: Usuarios
      },
      {
        path:'/clientes', 
        component:Clientes
      },
      {
        path:'/sedes', 
        component: Sedes
      },
      {
        path:'/rutas', 
        component:Rutas
      },
      {
        path:'/carteras',
        component: Carteras
      }
    ],
    beforeEnter: (to, from, next) => {
      
      if(localStorage.getItem("user")){
        next();
      } else {
        next({name:'login'});
      }
      
    }
  },
  {
    path: '/',
    name:'login',
    component: () => import('pages/login/Login.vue')
  },
  {
    path: '/forget',
    name:'forgetPassword',
    component: () => import('pages/forgetPas/forgetPassword.vue') 
  },
  {
    path: '/password-reset/:token?',
    name:'passwordReset',
    component: () => import('pages/forgetPas/assignPassword.vue')
  },
  {
    path: '/tokenerror',
    component: () => import('pages/forgetPas/tokenError.vue')
    
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
