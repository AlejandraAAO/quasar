export default [
    {
        numberOr: 1,
        nickname: 'ATUYA',
        names: 'Alan',
        lastNames: 'Tuya',
        typeDoc: 'DNI',
        numDoc: '123456',
        telephone: '999999',
        role: 'RTC',
        email: 'atuya@email.com',
        creationDate: '16/03/19',
        state: true
    },
    {
        numberOr: 2,
        nickname: 'NTUYA',
        names: 'Nugget',
        lastNames: 'Tuya',
        typeDoc: 'RUC',
        numDoc: '654892',
        telephone: '99925',
        role: 'Supervisor',
        email: 'ntuya@email.com',
        creationDate: '17/02/19',
        state: false
    }
]