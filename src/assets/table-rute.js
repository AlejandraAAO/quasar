export default [
    {
        number: 1,
        name: 'Lima',
        zoneSiteOrg: 6
    },
    {
        number: 2,
        name: 'Huacho',
        zoneSiteOrg: 2
    },
    {
        number: 3,
        name: 'Nugget',
        zoneSiteOrg: 3
    },
    {
        number: 4,
        name: 'Villa',
        zoneSiteOrg: 20
    },

]